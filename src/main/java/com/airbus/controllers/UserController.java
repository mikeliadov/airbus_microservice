package com.airbus.controllers;

import com.airbus.models.User;
import com.airbus.repositories.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ApplicationContext context;

    private final UserDao userDao;

    @Autowired
    public UserController(ApplicationContext context, UserDao userDao) {
        this.context = context;
        this.userDao = userDao;
    }

    @RequestMapping(method=RequestMethod.GET)
    public List<User> getAllUsers() {
        return userDao.findAll();
    }

    @RequestMapping(method=RequestMethod.POST)
    public User createUser(@Valid @RequestBody User user) {
        return userDao.save(user);
    }

    @RequestMapping(value="{id}", method=RequestMethod.GET)
    public ResponseEntity<User> getUserById(@PathVariable("id") String id) {
        User user = userDao.findOne(id);
        if(user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @RequestMapping(value="{id}", method=RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@Valid @RequestBody User user, @PathVariable("id") String id) {
        User userData = userDao.findOne(id);
        if(userData == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userData.setName(user.getName());
        userData.setEmail(user.getEmail());
        User updateUser = userDao.save(userData);
        return new ResponseEntity<>(updateUser, HttpStatus.OK);
    }

    @RequestMapping(value="{id}", method=RequestMethod.DELETE)
    public void deleteUser(@PathVariable("id") String id) {
        userDao.delete(id);
    }

    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public void uploadUsers(@RequestParam("file") MultipartFile file) {
        try{
            logger.info("uploadUsers");
            JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);
            jmsTemplate.convertAndSend("xmlUserImport", file.getBytes());
        } catch(Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

}
