package com.airbus.xml;

import com.airbus.models.User;
import com.airbus.repositories.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;

@Service
public class UserXMLService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserDao userDao;

    @Autowired
    public UserXMLService(UserDao userDao) {
        this.userDao = userDao;
    }

    public void importUsers(byte[] bytes) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new ByteArrayInputStream(bytes));

            doc.getDocumentElement().normalize();
            logger.info("importUsers started");

            NodeList nList = doc.getElementsByTagName("user");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    User user = new User();
                    user.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                    user.setEmail(eElement.getElementsByTagName("email").item(0).getTextContent());
                    user.setWebsite(eElement.getElementsByTagName("website").item(0).getTextContent());
                    user.setPhone(eElement.getElementsByTagName("phone").item(0).getTextContent());

                    User oldUser = userDao.findByEmail(user.getEmail());
                    if(oldUser == null) {
                        userDao.save(user);
                        logger.info("user imported: " + user);
                    } else {
                        oldUser.setName(user.getName());
                        oldUser.setWebsite(user.getWebsite());
                        oldUser.setPhone(user.getPhone());
                        userDao.save(oldUser);
                        logger.info("user updated: " + user);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }
}
