package com.airbus.repositories;

import com.airbus.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends MongoRepository<User, String> {
    List<User> findAll();
    User findOne(String id);
    User findByEmail(String email);
    User save(User user);
    void delete(User user);
}
