package com.airbus.queue;

import com.airbus.xml.UserXMLService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final UserXMLService userXMLService;

    @Autowired
    public Receiver(UserXMLService userXMLService) {
        this.userXMLService = userXMLService;
    }

    @JmsListener(destination = "xmlUserImport", containerFactory = "queueFactory")
    public void receiveMessage(byte[] bytes) {
        logger.info("Received new user import");
        userXMLService.importUsers(bytes);
        logger.info("Users imported");
    }
}
